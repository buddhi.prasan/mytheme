<?php get_header(); ?>

<article id="left">
  <?php while ( have_posts() ) : the_post(); ?>

  	<h2><?php the_title(); ?></h2>
    <?php the_date(); ?>

    <?php the_content(__('continue Reading')); ?>
  <?php endwhile; ?>
</article>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
